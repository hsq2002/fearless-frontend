window.addEventListener("DOMContentLoaded", async () => {

    const url = 'http://localhost:8000/api/states/';
    console.log(url)
    const response = await fetch (url);
    console.log(response)

    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById("state");

        console.log(data)
        for (let state of data.states) {
            const option = document.createElement("option")
            option.innerHTML = state.name;
            console.log(option.innerHTML);
            option.value = state.abbreviation;
            selectTag.appendChild(option);
            console.log(state)

        }
    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener("submit", async event => {
        event.preventDefault();
    const formData = new FormData(formTag)
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
    formTag.reset();
    const newLocation = await response.json();
    }

    });
}

});
